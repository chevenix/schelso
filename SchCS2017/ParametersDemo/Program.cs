﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParametersDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] reszek = new string[] { "alma", "béka", "cékla", "dézsa"};

            //string s = String.Empty;
            //foreach (string s1 in reszek)
            //{
            //    s += s1;
            //    s += " ";
            //}
            //s = s.Substring(0, s.Length - 1);
            ////s = s.Trim();
            //Console.WriteLine(s);

            //StringBuilder sb = new StringBuilder(30);
            //foreach (string s1 in reszek)
            //{
            //    sb.AppendFormat("{0} ", s1);
            //}
            //sb.Remove(sb.Length - 1, 1);
            //Console.WriteLine(sb.ToString());

            StringBuilder builder = new StringBuilder("alma");
            Console.WriteLine(builder);
            WriteBeka(ref builder);
            Console.WriteLine(builder);

            string s = "alma";
            int i;
            if (int.TryParse(s, out i))
            {
                Console.WriteLine(i);
            }

            Console.ReadLine();
        }

        private static void WriteBeka(ref StringBuilder sb)
        {
            sb = new StringBuilder();
            sb.Append("beka");
        }
    }
}
