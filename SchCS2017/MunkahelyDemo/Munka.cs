﻿using System;

namespace MunkahelyDemo
{
    class Munka
    {
        private string _leiras;
        public string Leiras
        {
            get { return _leiras; }
            set { _leiras = value; }
        }

        private string _jelleg;
        public string Jelleg
        {
            get { return _jelleg; }
            set { _jelleg = value; }
        }
    }
}
