﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MunkahelyDemo
{
    abstract class Melos
    {
    }

    class Bela : Melos, ITesztelo
    {
        public void Tesztel(Munka m)
        {
            Console.WriteLine("Én, Béla teszteltem ezt: " + m.Leiras);
        }
    }

    class Geza : Melos, ITesztelo, ITervezo
    {
        public void Tesztel(Munka m)
        {
            Console.WriteLine("Én, Géza teszteltem ezt: " + m.Leiras);
        }

        public void Tervez(Munka m)
        {
            Console.WriteLine("Én, Géza tervezem ezt: " + m.Leiras);

        }
    }
}