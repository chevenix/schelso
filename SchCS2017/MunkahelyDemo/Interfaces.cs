﻿namespace MunkahelyDemo
{
    interface IFejleszto
    {
        void Fejleszt(Munka m);
    }

    interface ITesztelo
    {
        void Tesztel(Munka m);
    }

    interface ITervezo
    {
        void Tervez(Munka m);
    }
}