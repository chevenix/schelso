﻿using System;
using System.Collections.Generic;

namespace MunkahelyDemo
{
    internal class Munkahely
    {
        private List<Melos> _melosok = new List<Melos>();

        public Munkahely()
        {
            _melosok.Add(new Bela());
            _melosok.Add(new Geza());
        }

        public void Vegrehajt(Munka m)
        {
            if (m.Jelleg == "fejleszt")
            {
                foreach (Melos ml in _melosok)
                {
                    if (ml is IFejleszto)
                    {
                        ((IFejleszto) ml).Fejleszt(m);
                        return;
                    }
                }
            }
            else if (m.Jelleg == "tervez")
            {
                foreach (Melos ml in _melosok)
                {
                    if (ml is ITervezo)
                    {
                        ((ITervezo) ml).Tervez(m);
                        return;
                    }
                }
            }
            else if (m.Jelleg == "tesztel")
            {
                foreach (Melos ml in _melosok)
                {
                    if (ml is ITesztelo)
                    {
                        ((ITesztelo) ml).Tesztel(m);
                        return;
                    }
                }
            }
            Console.WriteLine("Nem volt megfelelő ember.");
        }
    }
}