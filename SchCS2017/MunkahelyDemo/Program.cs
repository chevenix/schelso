﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Contexts;

namespace MunkahelyDemo
{
    class Program
    {
        static void Main()
        {
            Munkahely mh = new Munkahely();
            Munka m1 = new Munka();
            m1.Jelleg = "tesztel";
            m1.Leiras = "teszteni az osztályokat";
            mh.Vegrehajt(m1);

            Munka m2 = new Munka();
            m2.Jelleg = "tervez";
            m2.Leiras = "tervezni az osztályokat";
            mh.Vegrehajt(m2);

            Munka m3 = new Munka();
            m3.Jelleg = "fejleszt";
            m3.Leiras = "fejleszteni az osztályokat";
            mh.Vegrehajt(m3);

            Console.ReadLine();
        }
    }
}
