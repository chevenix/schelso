﻿using System;
using System.Linq;

namespace DelegateDemo
{
    delegate void VoidStringDelegate(string s);
    delegate int IntStringDelegate(string s);

    class Program
    {
        static void Main()
        {
            string[] ss = new string[] { "Alma", "Béka", "Cékla" };

            //Alapok(ss);
            AnonymousMethods(ss);

            Console.ReadLine();
        }

        private static void AnonymousMethods(string[] ss)
        {
            string prefix = "visszafelé: ";

            DoSomething(ss, delegate(string s)
            {
                Console.WriteLine(prefix + new string(s.Reverse().ToArray()));
            });

            VoidStringDelegate d1 = new VoidStringDelegate(
                delegate(string s)
                {
                    Console.WriteLine(s.Replace("a", "XXX"));
                });
            d1 += new StringMethods().PrintFirst;
            DoSomething(ss, d1);

            //DoSomething(ss, d1);
        }

        private static void Alapok(string[] ss)
        {
            StringMethods sm = new StringMethods();

            Action<string> a1 = sm.PrintFirst;
            Action<string, int> a2;

            Func<string> f1; //string M(){};
            Func<int, int, string> f2; //string M(int a, int b){};
            Func<string, int> f3;

            VoidStringDelegate d1 = sm.PrintFirst;
            d1 += sm.PrintUpper;
            d1 += sm.PrintFirst;
            d1 += sm.PrintUpper;
            d1 += sm.PrintObject;
            DoSomething(ss, d1); //írja ki az első betűket!

            Console.WriteLine();
            //VoidStringDelegate d3 = sm.PrintFirst;
            //d3 += sm.PrintFirst;
            //d1 = d1 - d3;

            DoSomething(ss, d1);

            Console.WriteLine();
            VoidStringDelegate d2 = sm.PrintLast;
            DoSomething(ss, d2); //írja ki az utolsó betűket!
        }

        private static void DoSomething(string[] ss, VoidStringDelegate m)
        {
            foreach (string s in ss)
            {
                m(s);
                //foreach (VoidStringDelegate d in m.GetInvocationList())
                //{
                //    d(s);
                //}
            }
        }
    }

    class StringMethods
    {
        public void PrintFirst(string s)
        {
            Console.WriteLine(s[0]);
        }

        public void PrintLast(string s)
        {
            Console.WriteLine(s[s.Length-1]);
        }

        public void PrintUpper(string s)
        {
            Console.WriteLine(s.ToUpper());
            throw new Exception();
        }

        public void PrintObject(object o)
        {
            Console.WriteLine(o.ToString());
        }
    }

}