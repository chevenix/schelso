﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;

namespace GenericsDemo
{
    class Program
    {
        static void Main()
        {
            //Pair();
            NullableTest();


            Console.ReadLine();
        }

        private static void NullableTest()
        {
            int i = 7;
            //i = null;
            Nullable<int> j = null;
            int? k = null;
            k++;
            Console.WriteLine(k);
            //i = (int)k;

            k = i;
            k++;
            Console.WriteLine(k);
            
        }

        private static void Pair()
        {
//Pair<int> p1 = new Pair<int>();
            //p1.X = 7;
            //p1.Y = 8;

            //Pair<string> p2 = new Pair<string>();
            //p2.X = "alma";
            //p2.Y = "béka";

            //Dictionary<string, double> d = new Dictionary<string, double>();
            //d.Add("nulla", 0.0);
            //d.Add("egy", 1.0);
            //d.Add("kettő", 2.0);


            //List<byte> bk = new List<byte>();
            //ArrayList al = new ArrayList();

            Pair<GreenApple> p3 = new Pair<GreenApple>();
            p3.X = new GreenApple(10, 2);
            p3.Y = new GreenApple(8, 3);

            Console.WriteLine(p3.GetSmaller().Diameter);
            Console.WriteLine(p3.GetSmallerUsingAMethod(
                new ComparerDelegate<GreenApple>(CompareGreenApples)).Diameter);

            int i = p3.Cast<int>(new CastTtoUDelegate<GreenApple, int>(
                CastGreenAppleToInt32));
            string s = p3.Cast(CastGreenAppleToString);
        }

        private static string CastGreenAppleToString(GreenApple t)
        {
            return "Ez egy zöldalma, "+ t.Size + " mérettel.";
        }

        private static int CastGreenAppleToInt32(GreenApple t)
        {
            return t.Diameter;
        }

        private static GreenApple CompareGreenApples(GreenApple a, GreenApple b)
        {
            return a.Diameter - b.Diameter < 0 ? a : b;
        }
    }
}