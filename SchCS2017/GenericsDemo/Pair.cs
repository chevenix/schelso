﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericsDemo
{
    class Pair<T>
        where T: Apple, IComparable<T>, new()
    {
        private T _x;
        public T X
        {
            get { return _x; }
            set { _x = value; }
        }

        private T _y;
        public T Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public T GetSmaller()
        {
            int result = _x.CompareTo(_y);
            if (result > 0) return _y;
            if (result < 0) return _x;
            return new T();
        }

        public T GetSmallerUsingAMethod(ComparerDelegate<T> d)
        {
            return d(_x, _y);
        }

        public T GetSmallerUsingAMethod(ComparerDelegateUsingPair<T> d)
        {
            return d(this);
        }


        public U Cast<U>(CastTtoUDelegate<T, U> d)
        {
            T t = this.GetSmaller();
            U result = d(t);
            return result;
        }
    }

    delegate T ComparerDelegate<T>(T a, T b);
    delegate T ComparerDelegateUsingPair<T>(Pair<T> p) 
        where T: Apple, IComparable<T>, new();

    internal delegate U CastTtoUDelegate<T, U>(T t);

}