﻿using System;

namespace GenericsDemo
{
    class Apple : IComparable<Apple>
    {
        private int _size;
        public int Size
        {
            get { return _size; }
            set { _size = value; }
        }

        public int CompareTo(Apple other)
        {
            if (other._size < this._size)
                return 1;
            if (other._size > this._size)
                return -1;
            return 0;
        }
    }

    class GreenApple : Apple, IComparable<GreenApple>
    {
        private int _diameter;
        public int Diameter
        {
            get { return _diameter; }
            set { _diameter = value; }
        }


        public GreenApple(int size, int diameter)
        {
            Size = size;
            Diameter = diameter;
        }

        public GreenApple()
        {
            
        }

        public int CompareTo(GreenApple other)
        {
            return base.CompareTo(other);
        }
    }
}