﻿using System;

namespace CounterDemo
{
    class Counter
    {
        public static int Count;

        public Counter()
        {
            Counter.Count++;
        }

        ~Counter()
        {
            Count--;
        }

        static Counter()
        {
            //statikus ctor, a statikus tagok egyszeri beállítására
            Count = 1;
        }
    }
}