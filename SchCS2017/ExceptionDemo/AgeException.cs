﻿using System;

namespace ExceptionDemo
{
    class AgeException : Exception
    {
        public AgeException() : base("Nem megfelelő érték.")
        {
        }

        public AgeException(string message) : base(message)
        {
        }

        public AgeException(string message, Exception inner) 
            : base(message, inner)
        {
        }
    }
}
