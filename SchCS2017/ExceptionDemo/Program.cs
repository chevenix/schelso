﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionDemo
{
    /// <summary>
    /// Ez a program osztály.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int age = GetAge();
                if (age < 18) Console.WriteLine("Gyerek vagy!");
                else Console.WriteLine("Nagyra nőtt gyerek vagy.");
            }
            catch (AgeException aex)
            {
                //Console.WriteLine(aex.InnerException.Message);
                Console.WriteLine("Hiba");
            }
            Console.ReadLine();
        }

        /// <summary>
        /// Bekéri és ellenőrzi az életkort.
        /// </summary>
        /// <exception cref="ExceptionDemo.AgeException">AgeException</exception>
        /// <returns></returns>
        private static int GetAge()
        {
            Console.Write("Add meg a korod: ");
            string s = Console.ReadLine();
            try
            {
                int i = int.Parse(s);
                if (i < 0 || i > 130)
                {
                    throw new AgeException();
                }
                return i;
            }
            catch (FormatException fex)
            {
                AgeException aex = new AgeException("Nem megfelelő érték.", fex);
                throw aex;
            }
            catch (OverflowException oex)
            {
                AgeException aex = new AgeException("Nem megfelelő érték.", oex);
                //aex.InnerException = oex;
                throw aex;
            }
            finally
            {
                Console.WriteLine("GetAge vége");
            }
        }
    }
}