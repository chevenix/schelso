﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorsDemo
{
    class Pill
    {
        private string _wineType;
        public string WineType
        {
            get { return _wineType; }
            set { _wineType = value; }
        }

        public Pill(string wineType)
        {
            _wineType = wineType;
        }

        public static implicit operator Wine(Pill p)
        {
            return new Wine(p._wineType, new Random().NextDouble() * 12);
        }

        public static explicit operator Pill(Wine w)
        {
            return new Pill(w.Type);
        }
    }


    internal class Wine
    {
        private string _type;

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private double _percent;

        public double Percent
        {
            get { return _percent; }
            set { _percent = value; }
        }

        public Wine(string type, double percent)
        {
            _type = type;
            _percent = percent;
        }

        public static Wine operator ++(Wine w)
        {
            w.Percent = w.Percent + 1;
            return w;
        }

        public static Wine operator +(Wine a, Wine b)
        {
            Wine result = new Wine(GetWineType(a.Type, b.Type), 
                (a.Percent + b.Percent) / 2);
            return result;
        }

        private static string GetWineType(string t1, string t2)
        {
            if (t1 == t2) return t1;
            return "kevert";
        }
    }
}
