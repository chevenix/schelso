﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorsDemo
{
    class Program
    {
        static void Main()
        {
            //int i = 8;
            //double d = 9.9;
            //d = (double)i;
            //i = (int)d;

            //int j = 9;
            //int k = i + j;
            //k++;

            Pill p1 = new Pill("vörös");
            Wine w1 = (Wine) p1;
            Console.WriteLine(w1.Type);
            Console.WriteLine(w1.Percent);

            w1++;
            Console.WriteLine(w1.Percent);

            Wine w2 = new Wine("fehér", 12.0);

            Wine w3 = w1 + w2;
            Console.WriteLine(w3.Type +  " " + w3.Percent);

            Console.ReadLine();
        }
    }
}