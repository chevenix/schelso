﻿using System;

namespace AlcoholDemo
{
    class Tree : IBurnable, IBurnable2
    {
        private int _temperature;
        public int Temperature
        {
            get { return _temperature; }
            set { _temperature = value; }   //nem kötelező
        }


        public void Burn()
        {
            Console.WriteLine("Ég a fa!");
        }

        //void IBurnable.Burn()
        //{
        //    Console.WriteLine("Ég a fa!");
        //}

        void IBurnable2.Burn()
        {
            Console.WriteLine("Ég a fa! KETTŐ!");
        }
    }
}