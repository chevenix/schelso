﻿using System;

namespace AlcoholDemo
{
    class Wine : Alcohol
    {
        public override void Drink()
        {
            Console.WriteLine("A bort borospohárból isszák.");
        }

        public override int Temperature
        {
            get { return 90; }
        }
    }

    //abstract class Pálinka : Alcohol
    //{
         
    //}
}
