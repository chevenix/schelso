﻿using System;

namespace AlcoholDemo
{
    partial class Alcohol
    {
        private int _volume;
        public int Volume
        {
            get { return _volume; }
            set
            {
                _volume = value;
            }
        }

    }
}
