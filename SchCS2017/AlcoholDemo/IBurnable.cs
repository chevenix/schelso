﻿using System;

namespace AlcoholDemo
{
    interface IBurnable
    {
        void Burn();
        //int M(string s);
        //int M(string s, int i);

        //event EventHandler MyEvent;

        int Temperature { get; }
    }

    interface IBurnable2
    {
        void Burn();
    }

    interface IVeryBurnable : IBurnable
    {
        //void Burn();
        //int Temperature { get; }
        void BurnVery();
    }
}