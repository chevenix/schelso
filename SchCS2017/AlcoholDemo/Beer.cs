﻿
using System;

namespace AlcoholDemo
{
    class Beer : Alcohol
    {
        private BeerColour _colour;
        public BeerColour Colour
        {
            get { return _colour; }
            set { _colour = value; }
        }

        public override double Percent
        {
            get { return _percent; }
            set
            {
                if (value < 0 || value > 30)
                {
                    throw new ArgumentException();
                }
                _percent = value;
            }
        }

        public override void Drink()
        {
            //base.Drink();
            Console.WriteLine("A sört korsóból isszák.");
        }
    }

    enum BeerColour : byte
    {
        Világos = 0, Barna = 2, Kék = 7
    }

    class FruitBeer : Beer
    {
        public override void Drink()
        {
            Console.WriteLine("A gyümölcsös sört dobozból isszák.");
        }

        public override string ToString()
        {
            return "Gyümölcsös sör.";
        }
    }
}