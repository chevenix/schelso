﻿using System;

namespace AlcoholDemo
{
    abstract partial class Alcohol : /*object, */IBurnable
    {
        protected double _percent = 7.7;

        public virtual double Percent
        {
            get { return _percent; }
            set
            {
                if (value > 0 && value <= MaxPercent)
                {
                    this._percent = value;
                    return;
                }
                throw new ArgumentException("nem jó érték");
            }
        }


        /// <summary>
        /// Összehasonlít két alkoholt, visszaadja az erősebbet.
        /// </summary>
        /// <param name="a">Az első paraméter.</param>
        /// <param name="b">A második paraméter.</param>
        /// <returns></returns>
        public static Alcohol GetStronger(Alcohol a, Alcohol b)
        {
            if (a.Percent > b.Percent) return a;
            return b;
        }

        public static Alcohol GetStronger(Alcohol a, Alcohol b, Alcohol c)
        {
            //TODO
            return null;
        }

        public static Alcohol GetStronger(params Alcohol[] alkoholok)
        {
            if (alkoholok == null) throw new ArgumentNullException(nameof(alkoholok));

            if (alkoholok.Length == 1) return alkoholok[0];

            if (alkoholok.Length == 2)
            {
                return GetStronger(alkoholok[0], alkoholok[1]);
                //return alkoholok[0].Percent > alkoholok[1].Percent ? alkoholok[0] : alkoholok[1];
            }

            Alcohol max = alkoholok[0];
            for (int i = 1; i < alkoholok.Length; i++)
            {
                if (max.Percent < alkoholok[i].Percent)
                {
                    max = alkoholok[i];
                }
            }
            return max;
        }


        //public static Alcohol GetStronger(Alcohol a, int b)
        //{
        //    if (a.Percent > b) return a;
        //    return null;
        //}


        //public const int MaxPercent = 96;
        public static readonly int MaxPercent;

        static Alcohol()
        {
            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
            {
                MaxPercent = 96;
            }
            else
            {
                MaxPercent = 20;
            }
        }

        public Alcohol()
        {
        }

        public Alcohol(double percent)
        {
            Percent = percent;
        }

        public abstract void Drink();
        //{
        //    Console.WriteLine("Az alkoholt vödörből isszák.");
        //}


        public void Burn()
        {
            Console.WriteLine("Ég az alkohol!");
        }

        public virtual int Temperature
        {
            get { return 100; }
        }

        //public abstract int Temperature { get; }
    }
}
