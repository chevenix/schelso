﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

namespace AlcoholDemo
{
    class Program
    {
        //http://bit.ly/2mAyA2U
        static void Main(string[] args)
        {
            //Tombok();
            //Gyujtemenyek();
            //Szviccs();
            //Ciklus();
            //Methods();
            //Enums();
            //MethodOverriding();
            //Interfaces();
            //StructInterface();
            TypeTest();
            Console.ReadLine();
        }

        private static void TypeTest()
        {
            Alcohol a = new Beer();
            //if (a is Beer)
            //{
            //    Console.WriteLine(((Beer)a).Colour);

            //    Beer b = (Beer)a;
            //    Console.WriteLine(b.Colour);
            //}

            Beer b = a as Beer;
            if (b != null)
            {
                Console.WriteLine(b.Colour);
            }
        }

        private static void StructInterface()
        {
            Value v = new Value(5);
            v.Increment();
            Console.WriteLine(v.X);
            Increment(v);
            Console.WriteLine(v.X);

        }

        private static void Increment(IIncrementable value)
        {
            value.Increment();
        }

        private static void Interfaces()
        {
            Alcohol a = new Beer();
            Tree t = new Tree();
            a.Burn();
            //t.Burn();
            t.Temperature = int.MaxValue;
            Console.WriteLine("A fa {0} fokon ég.", t.Temperature);
            BurnEverything(new IBurnable[] { a, t });

            IBurnable ib = a;
            ib = t;
            Console.WriteLine(ib.Temperature);
            //ib.Temperature = 30;
            ib = new FruitBeer();
            //ib = new IBurnable();

            //a.Temperature = 7;
            ib = a;
            //ib.Temperature = 7;

            IBurnable2 ib2 = new Tree();
            //ib2.Burn();

            Tree t2 = new Tree();
            //t2.Burn();
            IBurnable ib_A = t2;
            ib_A.Burn();
            IBurnable2 ib2_A = t2;
            ib2_A.Burn();
        }

        private static void BurnEverything(IBurnable[] t)
        {
            foreach (IBurnable b in t)
            {
                b.Burn();
            }
        }

        private static void MethodOverriding()
        {
            Alcohol a = new Beer();
            a.Drink();
        }

        private static void Enums()
        {
            Beer b = new Beer();
            b.Colour = BeerColour.Kék;
            Console.WriteLine(b.ToString());
            //Type t = b.GetType();
            //foreach (MethodInfo methodInfo in t.GetMethods())
            //{
            //    Console.WriteLine(methodInfo.Name);
            //}
        }

        private static void Methods()
        {
            //Alcohol a1 = new Alcohol();
            //a1.Percent = 60;
            //Alcohol a2 = new Alcohol(40);
            //Alcohol a3 = new Alcohol();
            //a3.Percent = 50;
            //Alcohol a4 = new Alcohol();
            //a4.Percent = 45;

            //a1.GetStronger(a1, a2);
            //a2.GetStronger(a1, a2);
            //Alcohol.GetStronger(a1, a2);
            ////a1.SetPercent(100);
            //a1.Percent = 100;
            //Console.WriteLine(a1.Percent);
            //Alcohol.GetStronger(a1, a2, a3, a4);

        }

        private static void Ciklus()
        {
            int k = 88;
            for (int j = 0; j < 10;)
            {
                Console.WriteLine(++j);
            }

            object[] ok = new object[] { 1,2,3 };

            int sum = 0;
            //for (int i = 0; i < ok.Length; i++)
            //{
            //    //sum = sum + ok[i];
            //    sum += (int)ok[i];
            //}
            foreach (int i in ok)
            {
                sum += i;
                //extract -= i;
            }
            Console.WriteLine(sum);
        }

        private static void Szviccs()
        {
            Console.Write("Mennyi?");
            int i = int.Parse(Console.ReadLine());
            switch (i)
            {
                case 0:
                case 1:
                    Console.WriteLine("kevés");
                    break;
                case 2:
                    Console.WriteLine("ez meg már sok");
                    break;
                default:
                    Console.WriteLine("nemtom");
                    break;
            }
        }

        private static void Gyujtemenyek()
        {
            List<int> intek = new List<int>();
            intek.Add(11);
            intek.Add(2);
            intek.Add(5);
            intek.Add(-99);
            intek[0] = 33;
            Console.WriteLine(intek[0]);

            Dictionary<string, double> dict = new Dictionary<string, double>();
            dict.Add("nulla", 0.0);
            dict.Add("egy", 1.0);
            dict.Add("kettő", 2.0);
            dict["három"] = 3.0;
            //dict.Add("három", 66);
            dict["három"] = 3.3;
            Console.WriteLine(dict["három"]);
            //Console.WriteLine(dict["négy"]);

            Stack<int> stack = new Stack<int>();
            stack.Push(10);
            stack.Push(11);
            stack.Push(12);
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());
        }

        private static void Tombok()
        {
            Alcohol[] alkoholok = new Alcohol[2];
            if (alkoholok[0] == null)
            {
                alkoholok[0] = new Beer();
            }
            Console.Write("Add meg a százalékot: ");
            string s = Console.ReadLine();
            double d = double.Parse(s);
            alkoholok[0].Percent = d;
            Console.WriteLine(alkoholok[0].Percent);
        }
    }
}
