﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructDemo
{
    class Program
    {
        static void Main()
        {
            Point p1 = new Point();
            p1.X = 8;
            p1.Y = 3;

            Point p2 = new Point();
            p2.X = 5;
            p2.Y = 1;

            p1 = p2;
            p2.X = 11;
            Console.WriteLine(p1.X);

            MyStruct ms = new MyStruct();
            MyClass mc = new MyClass();

            int i = 42;
            object o = new object();
            o = i;
            i++;
            int b = (int) o;
            b++;

            int[] intek = new int[] {1, 2, 3};
            object[] ok = new object[] {1,2,3};

            Console.ReadLine();
        }
    }

    struct Point
    {
        private int _x;
        public int X
        {
            get { return _x; }
            set { _x = value; }
        }
        public int Y;

        public Point(int x)
        {
            _x = x;
            Y = 7;
        }

    }

    //struct Point3D : Point
    //{
    //    public int Z;
    //}

    struct MyStruct
    {
        public Point P;
    }

    class MyClass
    {
        public Point P;
        public string S;
    }
}