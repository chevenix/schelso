﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CelebDemo
{
    class CelebList<T> : IEnumerable<T> 
        where T : Celeb
    {
        private T[] _items = new T[5];
        private int _nextIndex = 0;

        public void Add(T item)
        {
            if (_nextIndex >= _items.Length)
            {
                Array.Resize(ref _items, _items.Length * 2);
            }
            _items[_nextIndex] = item;
            _nextIndex++;
        }

        //public T Get(int index)
        //{
        //    if (index >= _nextIndex)
        //        throw new ArgumentOutOfRangeException("Ez az index még nem létezik.");
        //    return _items[index];
        //}

        public T this[int index]
        {
            get
            {
                if (index >= _nextIndex)
                    throw new ArgumentOutOfRangeException("Ez az index még nem létezik.");
                return _items[index];
            }
            set
            {
                if (index >= _nextIndex)
                    throw new ArgumentOutOfRangeException("Ez az index még nem létezik.");
                _items[index] = value;
            }
        }

        public T this[string index]
        {
            get
            {
                //TODO: név alapján megkeresni a celebet
                if (index == "első")
                {
                    return _items[0];
                }
                else if (index == "második")
                {
                    return _items[1];
                }
                else
                {
                    return _items[2];
                }
            }
        }

        public U Cast<U>(int index, CelebToSomethingDelegate<T, U> d)
        {
            if (index >= _nextIndex) throw new ArgumentOutOfRangeException("Nincs ilyen index.");
            return d(_items[index]);
        }

        //public CelebListIterator<T> GetEnumerator()
        //{
        //    return new CelebListIterator<T>(this);
        //}

        public IEnumerator<T> GetEnumerator()
        {
            //return new CelebList<T>.CelebListIterator<T>(this);
            yield return _items[1];
            yield return _items[0];
            yield return _items[2];
            for (int i = 0; i < _nextIndex; i++)
            {
                yield return _items[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        //public class CelebListIterator<T> : IEnumerator<T> 
        //    where T : Celeb
        //{
        //    private int _index = -1;
        //    private CelebList<T> celebList;

        //    public CelebListIterator(CelebList<T> celebList)
        //    {
        //        this.celebList = celebList;
        //    }

        //    public bool MoveNext()
        //    {
        //        return ++_index < celebList._nextIndex;
        //    }

        //    public void Reset()
        //    {
        //        _index = -1;
        //    }

        //    object IEnumerator.Current
        //    {
        //        get { return Current; }
        //    }

        //    public T Current
        //    {
        //        get { return celebList._items[_index]; }
        //    }

        //    public void Dispose()
        //    {
        //    }
        //}

    }




    delegate U CelebToSomethingDelegate<T, U>(T t)
        where T : Celeb;

    class Node<T>
    {
        private T _item;
        public T Item
        {
            get { return _item; }
            set { _item = value; }
        }

        private Node<T> _next;
        public Node<T> Next
        {
            get { return _next; }
            set { _next = value; }
        }

    }


    class RandomGenerator : IEnumerable<int>
    {
        Random r = new Random();

        public IEnumerator<int> GetEnumerator()
        {
            while (true)
            {
                int value = r.Next(0, 1000);
                if (value > 900)
                {
                    yield break;
                }
                yield return value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}