﻿using System;
using System.Collections.Generic;

namespace CelebDemo
{
    static class CelebSorter
    {
        public static T[] Sort<T, U>(T[] array, U comparer)
            where T : Celeb
            where U : IComparer<T>
        {
            T[] result = new T[array.Length];
            Array.Copy(array, result, array.Length);
            Array.Sort(result, comparer);
            return result;
        }

        public static T[] Sort<T>(T[] array, Comparison<T> comparer)
            where T : Celeb
        {
            T[] result = new T[array.Length];
            Array.Copy(array, result, array.Length);
            Array.Sort(result, comparer);
            return result;
        }
    }
}