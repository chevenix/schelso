﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CelebDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Interface();
            //Operator();
            //Typetest();
            //Delegates();
            //Events();
            //Generics();
            Iterator();

            string[] tiltottnevek = new string[]{ "Pákó", "Tünci", "Buci" };

            List<Celeb> celebek = new List<Celeb>();
            int i = 1;

            while (true)
            {
                Console.Write("Add meg a {0}. celeb nevét: ", i++);
                string nev = Console.ReadLine();

                if (nev == "ELÉG") break;

                if (tiltottnevek.Contains(nev))
                    nev = "Átlag celeb";

                //celebek.Add(new Celeb());
                //celebek[celebek.Count - 1].Name = nev;

                Celeb c = new ButaCeleb(nev, 50);
                celebek.Add(c);
            }

            foreach (Celeb celeb in celebek)
            {
                Console.WriteLine("Név: {0}, IQ: {1}.", celeb.Name, celeb.Iq);
            }

            Console.ReadLine();
        }

        private static void Iterator()
        {
            CelebList<Celeb> ck = new CelebList<Celeb>();
            ck.Add(new OkosCeleb());
            ck.Add(new OkosCeleb());
            ck.Add(new OkosCeleb());
            foreach (Celeb c in ck)
            {
                Console.WriteLine(c.Name);
            }

            var enumerator = ck.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current.Name);
            }

        }

        private static void Generics()
        {
            CelebList<Celeb> ck = new CelebList<Celeb>();
            ck.Add(new OkosCeleb());
            ck.Add(new OkosCeleb());
            ck.Add(new OkosCeleb());
            ck[0] = new ButaCeleb("Béka", 1);
            Console.WriteLine(ck["első"]);

            OkosCeleb[] okosok = new OkosCeleb[]
            {
                new OkosCeleb("Béka"), 
                new OkosCeleb("Alma")
            };
            OkosCeleb[] rendezett = CelebSorter.Sort(okosok, new OkosCelebSorter());
            foreach (OkosCeleb oc in rendezett)
            {
                Console.WriteLine(oc.Name);
            }
            //rendezett = CelebSorter.Sort(okosok, new OkosCelebSorter().Compare);
            rendezett = CelebSorter.Sort(okosok,
                delegate(OkosCeleb a, OkosCeleb b)
                {
                    return a.Iq - b.Iq;
                });
        }

        private static void Events()
        {
            SingleCell s1 = new SingleCell();
            s1.SizeChanged += S1_SizeChanged;
            s1.Size++;
        }

        private static void S1_SizeChanged(SingleCell s)
        {
            Console.WriteLine(s.Size);
        }

        private static void Delegates()
        {
            OkosCeleb oc = new OkosCeleb();
            ButaCeleb bc = new ButaCeleb("Béka", 1);

            //CelebChatMethods ccm = new CelebChatMethods();

            oc.Chat(bc, new CelebDelegate(CelebChatMethods.ChatAboutTheWeather));
            oc.Chat(bc, new CelebDelegate(CelebChatMethods.ChatAboutSports));
            bc.Chat(oc, CelebChatMethods.ChatAboutSports);
            bc.Chat(bc, CelebChatMethods.ChatAboutTheWeather);
        }

        private static void Typetest()
        {
            object o = 42;
            if (o is int)
            {
                int i = (int)o + 10;
                Console.WriteLine(i);
            }
            else
            {
                Console.WriteLine("nem integer volt az o-ban");
            }
        }

        private static void Operator()
        {
            SingleCell s1 = new SingleCell();
            s1.Size++;
            s1.Size++;
            s1.Size++;

            SingleCell s2 = new SingleCell();
            s2.Size = 14;
            SingleCell s3 = s1 + s2;
            Celeb c = (Celeb)s3;
            Console.WriteLine(c.Name + " " + c.Iq);
        }

        private static void Interface()
        {
            IBreathing[] légzőképesek = new IBreathing[]
            {
                new SingleCell(), new OkosCeleb()
            };
            for (int j = 0; j < légzőképesek.Length; j++)
            {
                légzőképesek[j].Breathe();
            }
        }
    }

}