﻿using System;
using System.Collections.Generic;

namespace CelebDemo
{
    internal class OkosCelebSorter :IComparer<OkosCeleb>
    {
        public int Compare(OkosCeleb x, OkosCeleb y)
        {
            return x.Name.CompareTo(y.Name);
        }
    }
}
