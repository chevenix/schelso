﻿using System;

namespace CelebDemo
{
    class OkosCeleb : Celeb
    {
        public OkosCeleb() : base("Alma", 80)
        {
        }

        public OkosCeleb(string name) : base(name, 80)
        {
        }

        private string _school;
        public string School
        {
            get { return _school; }
            set { _school = value; }
        }

        public override string Teach()
        {
            return Teach(Math.Min(Iq*2, 100));
        }
    }
}
