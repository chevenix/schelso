﻿using System;

namespace CelebDemo
{
    delegate void SizeChangedDelegate(SingleCell s);

    class SingleCell : IBreathing
    {
        private int _size = 1;
        public int Size
        {
            get { return _size; }
            set
            {
                _size = value;
                if (SizeChanged != null)
                {
                    SizeChanged(this);
                }
            }
        }

        public event SizeChangedDelegate SizeChanged;

        public void Breathe()
        {
            Console.WriteLine("Az egysejtű levegőt vett.");
        }

        public static SingleCell operator +(SingleCell a, SingleCell b)
        {
            SingleCell result = new SingleCell();
            result.Size = a.Size + b.Size;
            return result;
        }

        public static explicit operator Celeb(SingleCell c)
        {
            if (c.Size > 15)
            {
                OkosCeleb oc = new OkosCeleb("Egysejtű");
                oc.Iq = c.Size * 5;
                return oc;
            }
            else
            {
                return new ButaCeleb("Egysejtű", c.Size * 2);
            }
        }
    }
}
