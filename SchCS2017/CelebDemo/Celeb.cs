﻿using System;

namespace CelebDemo
{
    public abstract class Celeb : IBreathing
    {
        protected string _name;

        public string Name
        {
            get { return _name; }
        }

        protected int _iq = 50;

        public int Iq
        {
            get { return _iq; }
            set { _iq = value; }
        }

        private int _szerepIndex;

        public int SzerepIndex
        {
            get { return _szerepIndex; }
            protected set
            {
                if (value > _szerepIndex)
                {
                    _szerepIndex = value;
                }
            }
        }



        public Celeb(string name, int iq)
        {
            _name = name;
            Iq = iq;
        }


        public abstract string Teach();

        public string Teach(int newiq)
        {
            Iq = newiq;
            return String.Format("{0} IQ-ja: {1}", Name, Iq);
        }


        public virtual void SzerepelABlikkben()
        {
            Console.WriteLine("{0} szerepel a Blikkben.", Name);
            _szerepIndex++;
        }



        public static Celeb[] GetCelebs()
        {
            Celeb[] result = new Celeb[3];

            result[0] = new ButaCeleb("Béla", 90);
            result[1] = new ButaCeleb("Géza", 80);
            result[2] = new ButaCeleb("Zoli", 100);

            return result;
        }

        public static Celeb GetDumbest(Celeb a, Celeb b, params Celeb[] c)
        {
            //TODO: kivételkezelés

            if (c == null || c.Length == 0)
            {
                return a.Iq < b.Iq ? a : b;
            }

            Celeb result = a.Iq < b.Iq ? a : b;
            foreach (Celeb celeb in c)
            {
                if (result.Iq > celeb.Iq) result = celeb;
            }
            return result;
        }

        public void Breathe()
        {
            Console.WriteLine("{0} levegőt vett.", _name);
        }

        public void Chat(Celeb other, CelebDelegate d)
        {
            Console.WriteLine("Szervusz, {0}!", other.Name);
            d(other);
            Console.WriteLine("Viszlát, {0}!", other.Name);
        }
    }

    public delegate void CelebDelegate(Celeb c);

    static class CelebChatMethods
    {
        public static void ChatAboutTheWeather(Celeb c)
        {
            Console.WriteLine("Szép időnk van, nemde?");
        }

        public static void ChatAboutSports(Celeb c)
        {
            Console.WriteLine("Te mit sportolsz, {0}?", c.Name);
        }
    }
}