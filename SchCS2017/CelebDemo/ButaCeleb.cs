﻿
using System;

namespace CelebDemo
{
    /*sealed*/ class ButaCeleb : Celeb
    {
        public ButaCeleb(string name, int iq) : base(name, iq)
        {
        }

        private int _weeklyParties;
        public int WeeklyParties
        {
            get { return _weeklyParties; }
            set { _weeklyParties = value; }
        }


        public sealed override void SzerepelABlikkben()
        {
            Console.WriteLine("{0} szerepel a Blikkben, mert buta.", Name);
            SzerepIndex++;
            SzerepIndex--;      //valójában nem csökkenti az értéket
        }

        public override string Teach()
        {
            return Teach(Iq / 2);
        }
    }

    class Tünde : ButaCeleb
    {
        public Tünde(string name, int iq) : base(name, iq)
        {
        }

        //public override void SzerepelABlikkben()
        //{
        //    Console.WriteLine("hehehe");
        //}
    }
}
