﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventDemo
{
    class PulseChangedEventArgs : EventArgs
    {
        private int _previous;
        public int Previous
        {
            get { return _previous; }
        }

        private int _delta;
        public int Delta
        {
            get { return _delta; }
        }


        public PulseChangedEventArgs(int previous, int delta)
        {
            _previous = previous;
            _delta = delta;
        }
    }
}