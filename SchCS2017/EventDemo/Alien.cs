﻿using System;

namespace EventDemo
{
    class Alien
    {
        public void Spy(object c, PulseChangedEventArgs pcea)
        {
            Human h = c as Human;
            if (h != null)
            {
                Console.WriteLine(h.Name + " pulzusa: " + h.Pulse +
                ", előző: " + pcea.Previous);
            }
        }
    }
}