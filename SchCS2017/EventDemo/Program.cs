﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EventDemo
{
    class Program
    {
        static void Main()
        {
            Human c = new Human();
            c.Name = "Cartman";
            
            Human s = new Human();
            s.Name = "Stan";

            Alien a1 = new Alien();
            Alien a2 = new Alien();
            Jewsian j1 = new Jewsian();

            c.PulseChanged += a1.Spy;
            s.PulseChanged += a1.Spy;
            c.PulseChanged += a2.Spy;

            //c.PulseChanged += a1.Spy;
            //c.PulseChanged += a2.Spy;
            c.Pulse++;
            s.Pulse++;

            //c.PulseChanged(new Cartman());

            //c.PulseChanged -= a2.Spy;
            //c.PulseChanged += j1.React;
            //c.Pulse++;

            Console.ReadLine();
        }
    }
}