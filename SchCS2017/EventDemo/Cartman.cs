﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EventDemo
{
    //delegate void CartmanDelegate(Human c, PulseChangedEventArgs pcea);

    class Human
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        private int _pulse = 80;
        public int Pulse
        {
            get { return _pulse; }
            set
            {
                int previous = _pulse;
                int delta = value - previous;
                _pulse = value;
                if (PulseChanged != null)
                {
                    PulseChanged(this, new PulseChangedEventArgs(previous, delta));
                }
            }
        }

        public event EventHandler<PulseChangedEventArgs> PulseChanged;

    }
}